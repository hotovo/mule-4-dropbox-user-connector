import React from "react";
import "./header.css";

const Header = () => {
  return (
    <div className="header">
      <h1 style={{ maxWidth: "1240px", margin: "0 auto" }}>
        Dropbox Connector
      </h1>
    </div>
  );
};

export default Header;
