import React from "react";
import Layout from "./layout";
import Card from "./card";
import SimpleTable from "./simpleTable";
import { CopyBlock } from "react-code-blocks";
import "./example.css";

const compatibilities = [
  {
    id: 1,
    name: "App Name",
    description: "Dropbox app name for your account",
  },
  {
    id: 2,
    name: "Access Token",
    description: "Access token for the account you want to link",
  },
];


const createFolder = `<?xml version="1.0" encoding="UTF-8"?>

<mule xmlns:http="http://www.mulesoft.org/schema/mule/http" 
      xmlns:dropbox="http://www.mulesoft.org/schema/mule/dropbox"
	    xmlns="http://www.mulesoft.org/schema/mule/core"
      xmlns:doc="http://www.mulesoft.org/schema/mule/documentation" 
      xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" 
      xsi:schemaLocation="http://www.mulesoft.org/schema/mule/core http://www.mulesoft.org/schema/mule/core/current/mule.xsd
                          http://www.mulesoft.org/schema/mule/dropbox http://www.mulesoft.org/schema/mule/dropbox/current/mule-dropbox.xsd
                          http://www.mulesoft.org/schema/mule/http http://www.mulesoft.org/schema/mule/http/current/mule-http.xsd">

  <dropbox:config 
        name="Dropbox_Configuration" 
        doc:name="Dropbox Configuration"
  >
    <dropbox:connection accessToken="\${config.accessToken}" appName="\${config.appName}" />
  </dropbox:config>
  
  <http:listener-config 
        name="HTTP_Listener_config" 
        doc:name="HTTP Listener config"
  >
		<http:listener-connection host="0.0.0.0" port="8081" />
  </http:listener-config>
  
  <flow name="Create_Folder_Flow">
    <http:listener doc:name="Listener" config-ref="HTTP_Listener_config" path="/createFolder"/>
    <dropbox:create-folder doc:name="Files: Create folder" config-ref="Dropbox_Configuration" path="/createFolder"/>
    <logger level="INFO" doc:name="Logger" message="#[payload.name]"/>
  </flow>
  
</mule>
`;

const uploadFile = `<?xml version="1.0" encoding="UTF-8"?>

<mule xmlns:http="http://www.mulesoft.org/schema/mule/http" 
      xmlns:dropbox="http://www.mulesoft.org/schema/mule/dropbox"
	    xmlns="http://www.mulesoft.org/schema/mule/core"
      xmlns:doc="http://www.mulesoft.org/schema/mule/documentation" 
      xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" 
      xsi:schemaLocation="http://www.mulesoft.org/schema/mule/core http://www.mulesoft.org/schema/mule/core/current/mule.xsd
                          http://www.mulesoft.org/schema/mule/dropbox http://www.mulesoft.org/schema/mule/dropbox/current/mule-dropbox.xsd
                          http://www.mulesoft.org/schema/mule/http http://www.mulesoft.org/schema/mule/http/current/mule-http.xsd">

  <dropbox:config 
        name="Dropbox_Configuration" 
        doc:name="Dropbox Configuration"
  >
    <dropbox:connection accessToken="\${config.accessToken}" appName="\${config.appName}" />
  </dropbox:config>
  
  <http:listener-config 
        name="HTTP_Listener_config" 
        doc:name="HTTP Listener config"
  >
		<http:listener-connection host="0.0.0.0" port="8081" />
  </http:listener-config>
  
  <flow name="Create_Folder_Flow">
    <http:listener doc:name="Listener" config-ref="HTTP_Listener_config" path="/uploadFile"/>
    <dropbox:upload-file doc:name="Files: Upload file" config-ref="Dropbox_Configuration" path="/uploadFileFolder">
      <dropbox:file-content ><![CDATA[#["This can be Input Stream from file connector"]]]></dropbox:file-content>
    </dropbox:upload-file>
    <logger level="INFO" doc:name="Logger" message="#[payload.name]"/>
  </flow>
  
</mule>
`;

const deleteFolder = `<?xml version="1.0" encoding="UTF-8"?>

<mule xmlns:http="http://www.mulesoft.org/schema/mule/http" 
      xmlns:dropbox="http://www.mulesoft.org/schema/mule/dropbox"
	    xmlns="http://www.mulesoft.org/schema/mule/core"
      xmlns:doc="http://www.mulesoft.org/schema/mule/documentation" 
      xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" 
      xsi:schemaLocation="http://www.mulesoft.org/schema/mule/core http://www.mulesoft.org/schema/mule/core/current/mule.xsd
                          http://www.mulesoft.org/schema/mule/dropbox http://www.mulesoft.org/schema/mule/dropbox/current/mule-dropbox.xsd
                          http://www.mulesoft.org/schema/mule/http http://www.mulesoft.org/schema/mule/http/current/mule-http.xsd">

  <dropbox:config 
        name="Dropbox_Configuration" 
        doc:name="Dropbox Configuration"
  >
    <dropbox:connection accessToken="\${config.accessToken}" appName="\${config.appName}" />
  </dropbox:config>
  
  <http:listener-config 
        name="HTTP_Listener_config" 
        doc:name="HTTP Listener config"
  >
		<http:listener-connection host="0.0.0.0" port="8081" />
  </http:listener-config>
  
  <flow name="Create_Folder_Flow">
    <http:listener doc:name="Listener" config-ref="HTTP_Listener_config" path="/createFolder"/>
    <dropbox:delete doc:name="Files: Delete" config-ref="Dropbox_Configuration" path="/folderToDelete"/>
    <logger level="INFO" doc:name="Logger" message="#[payload.name]"/>
  </flow>
  
</mule>
`;

const Examples = () => {
  return (
    <Layout>
      <Card>
        <p>The following are common use cases for Dropbox User Connector:</p>
        <ul>
          <li className="type">Creating folder</li>
          <li className="type">Uploading file</li>
          <li className="type">Deleting folder</li>
        </ul>
      </Card>
      <Card>
        <h1 className="example-header">Create Folder</h1>
        <p>This example shows how to use Dropbox User connector to create a folder.</p>
        <h2 className="example-header">Flow for Creating a folder</h2>
        <p>The following screenshot shows an Anypoint Studio flow for creating folder:</p>
        <div style={{paddingTop:"15px"}}>
          <img src={require("../images/create_folder_flow.png")} alt="Create folder flow"/>
          <p>Figure 1. Use this sample flow to create a folder.</p>
        </div>
        

        <h2>Preliminary Steps</h2>
        <p>Follow these steps to start creating Dropbox folder:</p>
        <ol>
          <li className="type">Create a new Mule project in Studio.</li>
          <li className="type">
            Add the following properties to the <span className="properties">mule-artifact.properties</span> file to store your Dropbox User credentials:
            <div className="config">
              <p>config.appName=*****</p>
              <p>config.accessToken=*****</p>
            </div>
          </li>
          <li className="type">Place the <span className="properties">mule-artifact.properties</span> file in the project’s <span className="properties">src/main/resources</span> directory.</li>
        </ol>
      </Card>
      <Card>
        <h2>Configure HTTP Listener</h2>
        <p>Configure <span className="properties">HTTP Listener</span> to listen for HTTP requests on the <span className="properties">/createFolder</span> path:</p>
        <ol>
          <li className="type">In the Mule Palette view, search for <span className="properties">http</span> and select the Listener operation.</li>
          <li className="type">Drag the Listener operation onto the Studio canvas.</li>
          <li className="type">On the Listener tab, click the plus sign (+) next to the Connector configuration field to access the global element configuration fields for <span className="properties">HTTP Listener</span>.</li>
          <li className="type">
            In the Host field, select <span className="properties">localhost</span>:
            <div style={{paddingTop:"15px"}}>
              <img width="600px" src={require("../images/listener_config.png")} alt="Listener config"/>
              <p>Figure 2. Select localhost in the Host field to listen for HTTP requests on your local computer.</p>
            </div>
          </li>
          <li className="type">Click OK</li>
          <li className="type">
            In the Path field on the Listener tab, enter /createFolder:
            <div style={{paddingTop:"15px"}}>
              <img width="600px" src={require("../images/listener_tab.png")} alt="Listener tab"/>
            </div>
            <p>Figure 3. Enter /createFolder in the Path field to listen for HTTP requests on the /createFolder path.</p>
          </li>
        </ol>
      </Card>
      <Card>
        <h2>Configure the Create Folder Operation</h2>
        <ol>
          <li className="type">In the Mule Palette view, click Search in Exchange and search for dropbox user</li>
          <li className="type">Select Dropbox User Connector, click Add, and then click Finish.</li>
          <li className="type">Drag the Files: Create Folder operation to the right of Listener on the Studio canvas.</li>
          <li className="type">Click the Global Elements link below the flow.</li>
          <li className="type">Add the properties file you created earlier to the Configuration properties field, as described in <a
            className="anchor"
            href="https://docs.mulesoft.com/connectors/introduction/intro-connector-configuration-overview#property-placeholders"
            target="_blank"
            rel="noopener noreferrer"
          >
            Use Property Placeholders for Property Values.
          </a></li>
          <li className="type">Click Create.</li>
          <li className="type">Expand Connector Configuration.</li>
          <li className="type">Select Dropbox User Configuration and click OK.</li>
          <li className="type">
            Complete the following fields:
            <div>
              <SimpleTable 
                headers={[{ title: "Parameter" }, { title: "Description" }]}
                rows={compatibilities}
              />
              <p>The following screenshot shows an example of configuring a global element for Dropbox User Connector:</p>
            </div>
            <div style={{paddingTop:"15px"}}>
              <img width="600px" src={require("../images/dropbox_user_config.png")} alt="Dropbox user config"/>
              <p>Figure 4. Configure the credentials needed to access Dropbox User in the Connection section of the global element.</p>
            </div>
          </li>
          <li className="type">
            Click Test Connection to confirm that Mule can connect with the Amazon DynamoDB instance:
            <ul>
              <li className="type">If the connection is successful, click OK to save the configuration.</li>
              <li className="type">If the connection is unsuccessful, review or correct any incorrect parameters, and then test again.</li>
            </ul>
          </li>
          <li>Click OK.</li>
          <li className="type">
            On the Files: Create folder tab, configure the following fields:
            <ul>
              <li className="type">path - Path in the user's Dropbox</li>
              <li className="type">Auto rename - If there's a conflict, have the Dropbox server try to autorename the folder to avoid the conflict.</li>
            </ul>
            <p>
              The following screenshot shows an example of configuring the Create folder operation:
              <div style={{paddingTop:"15px"}}>
                <img width="600px" src={require("../images/folder_config.png")} alt="Folder Config"/>
                <p>Figure 5. Enter values for the Create folder operation fields.</p>
              </div>
            </p>
          </li>
        </ol>
      </Card>
      <Card>
        <h2>Configure a Logger Component</h2>
        <p>Configure a Logger component to print to the Mule console the response (in this case the name of the created folder) generated by the Create Folder operation:</p>
        <ol>
          <li className="type">In the Mule Palette view, search for logger.</li>
          <li className="type">Drag the Logger component to the right of Files: Create Folder on the Studio canvas.</li>
          <li className="type">Enter #[payload] in the Message field on the Logger tab.</li>
        </ol>
        <div style={{paddingTop:"15px"}}>
          <img width="600px" src={require("../images/logger.png")} alt="logger"/>
          <p>Figure 6. Enter #[payload] in the Message field to print the operation’s response to the Mule console.</p>
        </div>
      </Card>
      <Card>
        <h2>XML for the Complete Create Folder Example</h2>
        <div className="configure-code-block">
          <CopyBlock
            language={"jsx"}
            text={createFolder}
            wrapLines={true}
            codeBlock
          />
        </div>
        <h2>Run the Project</h2>
        <ol>
          <li className="type">In Package Explorer, right-click the project name and click Run As &rarr; Mule Application.</li>
          <li className="type">Open a browser and check the response after you enter the http://localhost:8081/createFolder URL. 
          You see the generated response from the Create folder operation in the Mule console.</li> 
        </ol>
      </Card>
      <Card>
        <h1>Upload File</h1>
        <p>This example shows how to use Dropbox User connector to upload a file.</p>
        <h2 className="example-header">Flow for uploading a file</h2>
        <p>The following screenshot shows an Anypoint Studio flow for upload a file:</p>
        <div style={{paddingTop:"15px"}}>
          <img src={require("../images/upload_file.png")} alt="Uplaod file flow"/>
          <p>Figure 7. Use this sample flow to upload a file.</p>
        </div>
      </Card>
      <Card>
        <h2>XML for the Complete Upload a file Example</h2>
        <div className="configure-code-block">
          <CopyBlock
            language={"jsx"}
            text={uploadFile}
            wrapLines={true}
            codeBlock
          />
        </div>
        <h2>Run the Project</h2>
        <ol>
          <li className="type">In Package Explorer, right-click the project name and click Run As &rarr; Mule Application.</li>
          <li className="type">Open a browser and check the response after you enter the http://localhost:8081/uploadFile URL. 
          You see the generated response from the Create folder operation in the Mule console.</li> 
        </ol>
      </Card>
      <Card>
        <h1>Delete Folder</h1>
        <p>This example shows how to use Dropbox User connector to Delete a folder.</p>
        <h2 className="example-header">Flow for deleting a folder</h2>
        <p>The following screenshot shows an Anypoint Studio flow for delete a folder:</p>
        <div style={{paddingTop:"15px"}}>
          <img src={require("../images/delete_folder.png")} alt="Delete folder flow"/>
          <p>Figure 8. Use this sample flow to delete a folder.</p>
        </div>
      </Card>
      <Card>
        <h2>XML for the Complete Delete Folder Example</h2>
        <div className="configure-code-block">
          <CopyBlock
            language={"jsx"}
            text={deleteFolder}
            wrapLines={true}
            codeBlock
          />
        </div>
        <h2>Run the Project</h2>
        <ol>
          <li className="type">In Package Explorer, right-click the project name and click Run As &rarr; Mule Application.</li>
          <li className="type">Open a browser and check the response after you enter the http://localhost:8081/deleteFolder URL. 
          You see the generated response from the Create folder operation in the Mule console.</li> 
        </ol>
      </Card>
    </Layout>
  );
};

export default Examples;
