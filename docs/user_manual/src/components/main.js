import React from "react";
import Layout from "./layout";
import Card from "./card";
import "./main.css";

const Main = () => {
  return (
    <Layout>
      <Card>
        <h4>Dropbox Connector v 1.0.0</h4>
        <p>
          In one sentence the Dropbox helps people be organized, stay focused,
          and get in sync with their teams.
        </p>
        <p>
          Dropbox Connector for Anypoint Studio provides an easy way to
          integrate with the Dropbox Service API, enabling Mule users to manage
          folders, files etc. without having to interact with the API directly.
          Dropbox Connector is built using the SDK for Java.
        </p>
        <p>
          Read through this user guide to understand how to set up and configure
          a basic flow using the connector. Track feature additions,
          compatibility, limitations and API version updates with each release
          of the connector using the Connector Release Notes. Review the
          connector operations and functionality using the Technical Reference
          alongside the demo application. MuleSoft maintains this connector
          under the MuleSoft Certified Category support policy.
        </p>
        <p>
          MuleSoft maintains this connector under the{" "}
          <a
            className="anchor"
            href="https://docs.mulesoft.com/mule-runtime/3.8/anypoint-connectors#connector-categories"
            target="_blank"
            rel="noopener noreferrer"
          >
            MuleSoft Certified Category
          </a>{" "}
          support policy.
        </p>
      </Card>
    </Layout>
  );
};

export default Main;
