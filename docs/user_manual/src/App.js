import React from "react";
import { Switch, Route } from "react-router-dom";
import Sidenav from "./components/sidenav";
import Configure from "./components/configure";
import Examples from "./components/examples";
import Prerequisites from "./components/prerequisites";
import Header from "./components/header";
import Footer from "./components/footer";
import Main from "./components/main";

import "./app.css";

function App() {
  return (
    <div className="grid-container">
      <Header />
      <Sidenav />
      <Switch>
        <Route path="/" exact render={Main} />
        <Route path="/prerequisites" component={Prerequisites} />
        <Route path="/configure" component={Configure} />
        <Route path="/examples" component={Examples} />
      </Switch>
      <Footer />
    </div>
  );
}

export default App;
