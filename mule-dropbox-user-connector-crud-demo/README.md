Dropbox User Connector Demo
==================================
## Introduction
This demo application shows the set of basic CRUD operations which are available by the Dropbox User Connector. These operations allow you to create folder, upload file, delete folder, create batch folder and delete batch folder.

## Prerequisites
* Java 8
* Anypoint Studio 7.x
* Mule Runtime  4.1.1 and later
* Dropbox credentials.

## Generate Access Token

**To create the Dropbox access token please follow the action steps described here :**
* Go to **https://www.dropbox.com/developers/apps/create** and authorize
* Choose an API - Dropbox API
* Choose the type of access - Full Dropbox
* Give your app a name

![Create_Dropbox_Account_App_Name](images/create-app-name.png)

* Scroll down to OAuth 2 section and hit the button *Generate*

![Create_Dropbox_Account_Access_Token](images/generate-access-token.png)

## Import the project
* Navigate to **File > Import**
* Select **Anypoint Studio Project from File System** (under the parent folder "Anypoint Studio")
* Provide the root path to the demo project folder (demo/dropbox-crud-demo).
* Select Mule Runtime
* Click **Finish**.
* Fill Dropbox credentials inside the properties file `src/main/resources/mule-artifact.properties`. Fill out the following details:

```
config.appName=
config.accessToken=
```

## Configuration (Anypoint Studio)
* Open the **Global Elements** tab and click on **Create**.
* Find and select the **Dropbox Configuration**.
* Fill out your Dropbox access token and app name.

![Demo Dropbox_Configuration](images/dropbox-config.png)

* Click the **Test Connection** button to ensure there is connectivity with the Dropbox. A successful message should pop-up.

* Open a browser and access the URL **http://localhost:8081**. You should see the demo application deployed:

## Run the demo

### Create Folder

![Demo Create_Folder](images/create-folder.png)

* Click the button **Create Folder** and wait a few moments to finish processing.
* You should see the alert box with success message. 

### Upload File	

![Demo Upload_File](images/upload-file.png)

* Click the button **Upload File** and wait a few moments to finish processing.
* You should see the alert box with success message. 

###  Delete Folder

![Demo Delete_Folder](images/delete-folder.png)

* Click the button **Delete Folder** and wait a few moments to finish processing.
* You should see the alert box with success message.

###  Create Folder Batch

![Demo Create_Folder_Batch](images/create-folder-batch.png)

* Click the button **Create Folder Batch** and wait a few moments to finish processing.
* You should see the alert box with success message.

###  Delete Folder Batch

![Demo Delete_Folder_Batch](images/delete-folder-batch.png)

* Click the button **Delete Folder Batch** and wait a few moments to finish processing.
* You should see the alert box with success message.

## See more
* For additional technical information on the Dropbox Connector, visit the [technical reference documentation](something).
* For more information on the Dropbox API, go to the [Dropbox API documentation page](https://www.dropbox.com/developers/documentation/http/documentation).